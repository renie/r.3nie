/**
 * Uses the selected language, to load correct translations
 *
 * @async
 *
 * @returns {Object}
 *
 * @memberof Helpers.I18N
 */
const init = async (lang) => {
    const translations = (await import(`./t_${lang}.js`)).default
    return {
        /**
        * Gets the translation map, of a specific context,
        * in the previously selected language.
        *
        * @param {string} context - The translation context name
        *
        * @example 
        * // returns "contato" if selected language is pt-BR, "contact" if en-US
        * translate('mainmenu').contact
        *
        * @returns {Array} list of a context's tranlations
        *
        * @memberof Helpers.I18N
        */
        translate: (context) => {
            if (!Object.keys(translations).includes(context)) return []
            return translations[context]
        }
    }
}

const languagesAvailable = ['en', 'pt']

const langExists = (lang) => languagesAvailable.includes(lang)


/** @namespace Helpers.I18N */
export {
    init,
    langExists,
    languagesAvailable
}
