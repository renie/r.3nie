import { init as initI18N } from './i18n'

const JSONLDTag = `
    <script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "Person",
        "name": "Renie",
        "jobTitle": "Software Developer",
        "email": "mailto:reniedev.site@3nie.com",
        "nationality": "brazilian",
        "image": "https://3nie.com/assets/me_art-q_7e-izX.webp",
        "url": "https://3nie.com"
    }
    </script>
`

const createLinkTags = (page) => {
    return `
        ${page === 'home'
            ? `<link rel="canonical" href="${`https://3nie.com/`}" />`
            : `<link rel="canonical" href="${`https://3nie.com/en/${page}`}" />`}
        ${page === 'home'
            ? `<link rel="alternative" hreflang='en' href="${`https://3nie.com/en/home`}" />
               <link rel="alternative" hreflang='pt' href="${`https://3nie.com/pt/home`}" />`
            : `<link rel="alternative" hreflang="pt" href="${`https://3nie.com/pt/${page}`}" />`}
    `
}

const defaultHead = (page) => async (lang) => {
    const contexts = (await initI18N(lang))
    const i18n = contexts.translate(page)
    return `
        <title>${i18n.metatitle}</title>
        <meta name="description" content="${i18n.metadescription}" />
        <meta name="keywords" content="${i18n.metakeywords}" />
        <meta name="author" content="Renie" />
        ${JSONLDTag}
        ${createLinkTags(page)}
    `
}

const defaultRender = (html, head) => async (_,__,lang) => ({
    html: await html(lang),
    head: await head(lang)
})

const pageExists = (page) =>
    ([
        'home',
        'contact',
        'projects',
        'e404',
        'e500'
    ]).includes(page)

export {
    defaultHead,
    defaultRender,
    pageExists
}
