import { describe, it, vi } from 'vitest'
import { html2json } from 'html2json'
import { pageExists, defaultRender, defaultHead } from './page'

describe.concurrent('Page :: pageExists', () => {
    it('Should exist', async({ expect }) => {
        expect(pageExists('home')).toBeTruthy()
    })
 
    it('Should not exist', async({ expect }) => {
        expect(pageExists('homes')).toBeFalsy()
    })
 
    it('Edge case: null', async({ expect }) => {
        expect(pageExists(null)).toBeFalsy()
    })

    it('Edge case: empty string', async({ expect }) => {
        expect(pageExists('')).toBeFalsy()
    })
})

describe.concurrent('Page :: defaultRender', () => {
    it('Should just work', async ({ expect }) => {
        const langDefined = 'en'
        const htmlFn = vi.fn(async (lang) => Promise.resolve(lang))
        const headFn = vi.fn(async (lang) => Promise.resolve(lang))

        const renderFn = defaultRender(htmlFn, headFn)
        expect(htmlFn).not.toHaveBeenCalled()
        expect(headFn).not.toHaveBeenCalled()

        const createdObj = await renderFn(null, null, langDefined)
        expect(createdObj).toBeTypeOf('object')
        expect(htmlFn).toHaveBeenCalledTimes(1)
        expect(headFn).toHaveBeenCalledTimes(1)
        expect(createdObj.html).toBe(langDefined)
        expect(createdObj.head).toBe(langDefined)
    })
})

describe.concurrent('Page :: defaultHead', () => {
    it('Not existent page :: description tag should be empty', async ({ expect }) => {
        const headFn = defaultHead('bla')
        const htmlString = await headFn('en')
        const html = html2json(htmlString).child

        expect(html).not.toBeNull()
        const elements = html.filter(node => node.node === 'element')

        expect(elements.filter(node => node.tag === 'title')).toHaveLength(1)
        expect(elements.filter(node => node.tag === 'meta' && node.attr.name === 'description' )).toHaveLength(1)
        expect(elements.filter(node => node.tag === 'meta'
            && node.attr.name === 'description'
            && node.attr.content === 'undefined')).toHaveLength(1)
        expect(elements.filter(node => node.tag === 'link' && node.attr.rel === 'canonical')).toHaveLength(1)
        expect(elements.filter(node => node.tag === 'link' && node.attr.rel === 'alternative')).toHaveLength(1)
        expect(elements).toHaveLength(7)
    })

    it('Existent page :: description should have some text', async ({ expect }) => {
        const pageName = 'contact'
        const headFn = defaultHead(pageName)
        const htmlString = await headFn('en')
        const html = html2json(htmlString).child

        expect(html).not.toBeNull()
        const elements = html.filter(node => node.node === 'element')

        expect(elements.filter(node => node.tag === 'title')).toHaveLength(1)
        expect(elements.filter(node => node.tag === 'meta'
            && node.attr.name === 'description'
            && node.attr.content !== 'undefined')).toHaveLength(1)
        expect(elements.filter(node => node.tag === 'link'
            && node.attr.rel === 'canonical'
            && node.attr.href.includes(pageName))).toHaveLength(1)
        expect(elements.filter(node => node.tag === 'link'
            && node.attr.rel === 'alternative'
            && node.attr.href.includes(pageName)
            && node.attr.href.includes('pt'))).toHaveLength(1)
        expect(elements).toHaveLength(7)
    })

    it('Special page :: just home page should have 2 alternative links', async ({ expect }) => {
        const pageName = 'home'
        const headFn = defaultHead(pageName)
        const htmlString = await headFn('en')
        const html = html2json(htmlString).child

        expect(html).not.toBeNull()
        const elements = html.filter(node => node.node === 'element')
        expect(elements.filter(node => node.tag === 'title')).toHaveLength(1)
        expect(elements.filter(node => node.tag === 'meta'
            && node.attr.name === 'description'
            && node.attr.content !== 'undefined')).toHaveLength(1)
        expect(elements.filter(node => node.tag === 'link'
            && node.attr.rel === 'canonical'
            && !node.attr.href.includes(pageName))).toHaveLength(1)
        expect(elements.filter(node => node.tag === 'link'
            && node.attr.rel === 'alternative'
            && node.attr.href.includes(pageName)
            && node.attr.href.includes('pt'))).toHaveLength(1)
        expect(elements.filter(node => node.tag === 'link'
            && node.attr.rel === 'alternative'
            && node.attr.href.includes(pageName)
            && node.attr.href.includes('en'))).toHaveLength(1)
        expect(elements).toHaveLength(8)
    })
})
