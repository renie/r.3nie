import helmet from 'helmet'
import responseTime from 'response-time'
import { langExists, languagesAvailable } from './i18n'
import { pageExists } from './page'

const helmetDirective = {
    contentSecurityPolicy: {
        directives: {
            connectSrc: ["'self' ws:"]
        },
    },
}

// TODO: this function deserves a nice refactoring
const languageMiddleware = (req, _ , next) => {
    const defaultPage = 'home'
    if (req.path === '/') {
        req.nextPage = defaultPage
    } else {
        const [ __, p1, p2 ] = req.path.split('/')
        if (langExists(p1)) req.lang = p1
        if (!req.lang && pageExists(p1)) req.nextPage = p1
        if (req.lang && pageExists(p2)) req.nextPage = p2
        if (!req.nextPage) req.error = true
    }

    if (!req.lang)
        req.lang = req.acceptsLanguages(...languagesAvailable) || 'en'
    
    next()
    return
}


const prodServer = async (app, env) => {
    const compression = (await import('compression')).default
    const sirv = (await import('sirv')).default
    const secondsInAnHour = 3600
    const aBitLessThanADay = secondsInAnHour * 23.5
    
    app.use(languageMiddleware)
    app.use(compression())
    app.use(helmet(helmetDirective))
    app.use(env.base, sirv('./dist/server', { extensions: [], gzip: true, maxAge: aBitLessThanADay }))
    app.use(responseTime())
    return { app }
}

const devServer = async (app, viteServer) => {
    app.use(viteServer.middlewares)
    app.use(languageMiddleware)
    app.use(helmet(helmetDirective))
    app.use(responseTime())
    return { app }
}

const addAll = async (app, env, viteServer) => (
    !env.isProduction
        ? await devServer(app, viteServer)
        : await prodServer(app, env))

export {
    addAll
}
