import fs from 'node:fs/promises'

const removeSpaces = (template) =>
    template
        .replaceAll(/\n/g, '' )
        .replaceAll(/(\s){2,}/g, ' ')
        .replaceAll('> <', '><')

const replacePlaceholders = (template, rendered, lang) =>
    template
        .replace('<!--app-head-->', rendered.head ?? '')
        .replace('<!--app-html-->', rendered.html ?? '')
        .replace('<html lang="en">', `<html lang="${lang}">`)

const prepareHtml = (template, rendered, lang) =>
    removeSpaces(replacePlaceholders(template, rendered, lang))

const templateHtml = async (env) => (env.isProduction
    ? await fs.readFile('./dist/client/index.html', 'utf-8')
    : '')

const ssrManifest = async (env) => (env.isProduction
    ? await fs.readFile('./dist/server/.vite/ssr-manifest.json', 'utf-8')
    : undefined)

const getDevTemplate = async (url, page, vite) => {
    try {
        const rawTemplate = await fs.readFile('./index.html', 'utf-8')
        const template = await vite.transformIndexHtml(url, rawTemplate)
        const pages = await import('../pages.js')
        if (!Object.keys(pages).includes(page))
            throw Error(`Page not found: ${page}`)

        return { template, render: pages[page] }
    } catch(err) {
        return { err }
    }
}

const getProdTemplate = async (env, page) => {
    try {
        const template = await templateHtml(env)
        const pages = await import(`../pages.js`)

        if (!Object.keys(pages).includes(page))
            throw Error(`Page not found: ${page}`)

        return { template, render: pages[page] }
    } catch(err) {
        console.log(err)
        return { err }
    }
}

const getAssets = async (env, url, page, vite) => {
    return (!env.isProduction
        ? {...(await getDevTemplate(url, page, vite)), ssrManifest: await ssrManifest(env)}
        : {...(await getProdTemplate(env, page)), ssrManifest: await ssrManifest(env)})
}

export {
    getAssets,
    prepareHtml
}
