export default {
    isProduction: (process.env.NODE_ENV === 'production'),
    port: (process.env.PORT || 5173),
    base: (process.env.BASE || '/')
}
