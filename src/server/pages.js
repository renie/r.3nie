import { render as home } from './pages/home.js'
import { render as contact } from './pages/contact.js'
import { render as projects } from './pages/projects.js'
import { render as e404 } from './pages/404.js'
import { render as e500 } from './pages/500.js'

export {
    home,
    contact,
    projects,
    e404,
    e500
}
