import { Header, Footer } from '../components'
import stylesheet from './home.module.css?inline'
import styles from './home.module.css'
import image from '../../img/me_art.webp'
import { init as initI18N } from '../helpers/i18n'
import { defaultHead, defaultRender } from '../helpers/page'

const pageName = 'home'

const html = async (lang) => {
    const contexts = (await initI18N(lang))
    const i18n = contexts.translate(pageName)
    return `
        ${Header.create(contexts, 'who-am-i', lang)}
        
        <style>${stylesheet}</style>
        <main>        
            <section
                id="who-am-i"
                class="${styles.whoamiContainer} h-card vcard">

                <div>
                    <h1>
                        <a href="#who-am-i">${i18n.whoami}</a>
                    </h1>
                    <p>${i18n.greet_name_profession}</p>
                    <p>${i18n.what_can_be_found_here}</p>
                </div>

                <img
                    class="u-photo photo"
                    src="${image}"
                    alt="${i18n.pic_alt}"/>
            
            </section>
        </main>

        ${await Footer.create(contexts, lang, pageName)}
    `
}

const render = defaultRender(html, defaultHead(pageName))

export {
    render
}

