import { Header, Footer } from '../components'
import stylesheet from './404.module.css?inline'
import styles from './404.module.css'
import { init as initI18N } from '../helpers/i18n'
import { defaultRender, defaultHead } from '../helpers/page'

const html = async (lang) => {
    const contexts = (await initI18N(lang))
    const i18n = contexts.translate('404')
    return `
        ${Header.create(contexts, 'error-message', lang)}

        <style>${stylesheet}</style>
        <main>        
            <section
                id="error-message"
                class="${styles.errorContainer}">               

                <div>
                    <h1>
                        <a href="#error-message">${i18n.title}</a>
                    </h1>
                    <p>${i18n.message}</p>
                    <small>${i18n.code}</small>
                </div>
            </section>
        </main>

        ${await Footer.create(contexts, lang)}
    `
}

const render = defaultRender(html, defaultHead('404'))

export {
    render
}
