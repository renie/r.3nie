import { Header, Footer } from '../components'
import stylesheet from './projects.module.css?inline'
import styles from './projects.module.css'
import { init as initI18N } from '../helpers/i18n'
import { defaultRender, defaultHead } from '../helpers/page'

const pageName = 'projects'

const externalLinkSVG = (i18n) => `
    <svg width="12" height="12" viewBox="0 0 24 24">
        <title>${i18n.externallinks}</title>
        <path d="M14,3V5H17.59L7.76,14.83L9.17,16.24L19,6.41V10H21V3M19,19H5V5H12V3H5C3.89,3 3,3.9 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V12H19V19Z"></path>
    </svg>
`

const html = async (lang) => {
    const contexts = (await initI18N(lang))
    const i18n = contexts.translate(pageName)
    const externalLinkIcon = externalLinkSVG(i18n)

    return `
        ${Header.create(contexts, 'projects-and-services', lang)}
        
        <style>${stylesheet}</style>
        <main>        
            <section
                id="projects-and-services"
                class="${styles.projectsAndServices}">
               
                <h1>
                    <a href="#projects-and-services">${i18n.projects}</a>
                </h1>
                <p>${i18n.projectsDescription}<p>
                
                <ul>
                    <li>
                        <h3>${i18n.projects_3nie_title}</h3>
                        <dl>
                            <dt>${i18n.projects_date}</dt>
                            <dd>${i18n.projects_3nie_date}</dd>
                            <dt>${i18n.projects_desc}</dt>
                            <dd>${i18n.projects_3nie_desc}</dd>
                            <dt>${i18n.projects_stack}</dt>
                            <dd>${i18n.projects_3nie_stack}</dd>
                            <dt>${i18n.projects_url}</dt>
                            <dd><a href="https://gitlab.com/renie/r.3nie" rel="nofollow">gitlab.com/renie/r.3nie ${externalLinkIcon}</a></dd>
                        </dl>
                    </li>
                    <li>
                        <h3>${i18n.projects_lorehub_title}</h3>
                        <dl>
                            <dt>${i18n.projects_date}</dt>
                            <dd>${i18n.projects_lorehub_date}</dd>
                            <dt>${i18n.projects_desc}</dt>
                            <dd>${i18n.projects_lorehub_desc}</dd>
                            <dt>${i18n.projects_stack}</dt>
                            <dd>${i18n.projects_lorehub_stack}</dd>
                            <dt>${i18n.projects_url}</dt>
                            <dd>${i18n.projects_lorehub_url}</a></dd>
                        </dl>
                    </li>
                    <li>
                        <h3>${i18n.projects_mojito_title}</h3>
                        <dl>
                            <dt>${i18n.projects_date}</dt>
                            <dd>${i18n.projects_mojito_date}</dd>
                            <dt>${i18n.projects_desc}</dt>
                            <dd>${i18n.projects_mojito_desc}</dd>
                            <dt>${i18n.projects_stack}</dt>
                            <dd>${i18n.projects_mojito_stack}</dd>
                            <dt>${i18n.projects_url}</dt>
                            <dd><a href="https://github.com/mintlayer/mojito-browser-extension" rel="nofollow">github.com/mintlayer/mojito-browser-extension ${externalLinkIcon}</a></dd>
                        </dl>
                    </li>
                    <li>
                        <h3>${i18n.projects_entropy_title}</h3>
                        <dl>
                            <dt>${i18n.projects_date}</dt>
                            <dd>${i18n.projects_entropy_date}</dd>
                            <dt>${i18n.projects_desc}</dt>
                            <dd>${i18n.projects_entropy_desc}</dd>
                            <dt>${i18n.projects_stack}</dt>
                            <dd>${i18n.projects_entropy_stack}</dd>
                            <dt>${i18n.projects_url}</dt>
                            <dd><a href="https://www.npmjs.com/package/@mintlayer/entropy-generator" rel="nofollow">www.npmjs.com/package/@mintlayer/entropy-generator ${externalLinkIcon}</a></dd>
                        </dl>
                    </li>
                    <li>
                        <h3>${i18n.projects_exodus_title}</h3>
                        <dl>
                            <dt>${i18n.projects_date}</dt>
                            <dd>${i18n.projects_exodus_date}</dd>
                            <dt>${i18n.projects_desc}</dt>
                            <dd>${i18n.projects_exodus_desc}</dd>
                            <dt>${i18n.projects_stack}</dt>
                            <dd>${i18n.projects_exodus_stack}</dd>
                            <dt>${i18n.projects_url}</dt>
                            <dd><a href="https://www.exodus.com/mobile/" rel="nofollow">www.exodus.com/mobile ${externalLinkIcon}</a></dd>
                        </dl>
                    </li>
                </ul>

                <h2 id="opensource">
                    <a href="#opensource">${i18n.opensource}</a>
                </h2>
                <p>${i18n.opensourceDescription}<p>
                <ul>
                    <li>
                        <h3>TLDR</h3>
                        <dl>
                            <dt>${i18n.opensource_desc}</dt>
                            <dd>${i18n.opensource_tldr_desc}</dd>
                            <dt>${i18n.opensource_role}</dt>
                            <dd>${i18n.opensource_tldr_role}</dd>
                            <dt>${i18n.opensource_link}</dt>
                            <dd><a href="https://tldr.sh/" rel="nofollow">tldr.sh/ ${externalLinkIcon}</a></dd>
                            <dt>${i18n.opensource_url}</dt>
                            <dd><a href="https://github.com/tldr-pages" rel="nofollow">github.com/tldr-pages ${externalLinkIcon}</a></dd>
                        </dl>
                    </li>
                </ul>

                <h2 id="services">
                    <a href="#services">${i18n.services}</a>
                </h2>
                <p>${i18n.servicesDescription}<p>
                <p>${i18n.servicesDisclaimer}<p>
                
                <ul>
                    <li>
                        <h3>SearX</h3>
                        <dl>
                            <dt>${i18n.services_desc}</dt>
                            <dd>${i18n.services_searx_desc}</dd>
                            <dt>${i18n.services_link}</dt>
                            <dd><a href="https://searx.3nie.com/" rel="nofollow">searx.3nie.com/ ${externalLinkIcon}</a></dd>
                            <dt>${i18n.services_url}</dt>
                            <dd><a href="https://github.com/searx" rel="nofollow">github.com/searx ${externalLinkIcon}</a></dd>
                        </dl>
                    </li>
                    <li>
                        <h3>Lingva Translate</h3>
                        <dl>
                            <dt>${i18n.services_desc}</dt>
                            <dd>${i18n.services_lingva_desc}</dd>
                            <dt>${i18n.services_link}</dt>
                            <dd><a href="https://translate.3nie.com" rel="nofollow">translate.3nie.com ${externalLinkIcon}</a></dd>
                            <dt>${i18n.services_url}</dt>
                            <dd><a href="https://github.com/thedaviddelta/lingva-translate" rel="nofollow">github.com/thedaviddelta/lingva-translate ${externalLinkIcon}</a></dd>
                        </dl>
                    </li>
                    <li>
                        <h3>Shorturl (Polr)</h3>
                        <dl>
                            <dt>${i18n.services_desc}</dt>
                            <dd>${i18n.services_polr_desc}</dd>
                            <dt>${i18n.services_link}</dt>
                            <dd><a href="https://l.3nie.com/" rel="nofollow">l.3nie.com ${externalLinkIcon}</a></dd>
                            <dt>${i18n.services_url}</dt>
                            <dd><a href="https://github.com/cydrobolt/polr" rel="nofollow">github.com/cydrobolt/polr ${externalLinkIcon}</a></dd>
                        </dl>
                    </li>
                    <li>
                        <h3>Microbin</h3>
                        <dl>
                            <dt>${i18n.services_desc}</dt>
                            <dd>${i18n.services_bin_desc}</dd>
                            <dt>${i18n.services_link}</dt>
                            <dd><a href="https://bin.3nie.com/" rel="nofollow">bin.3nie.com ${externalLinkIcon}</a></dd>
                            <dt>${i18n.services_url}</dt>
                            <dd><a href="https://github.com/szabodanika/microbin" rel="nofollow">github.com/szabodanika/microbin ${externalLinkIcon}</a></dd>
                        </dl>
                    </li>
                    <li>
                        <h3>Jitsi</h3>
                        <dl>
                            <dt>${i18n.services_desc}</dt>
                            <dd>${i18n.services_polr_desc}</dd>
                            <dt>${i18n.services_link}</dt>
                            <dd><a href="https://chat.3nie.com/" rel="nofollow">chat.3nie.com ${externalLinkIcon}</a></dd>
                            <dt>${i18n.services_url}</dt>
                            <dd><a href="https://github.com/jitsi" rel="nofollow">github.com/jitsi ${externalLinkIcon}</a></dd>
                        </dl>
                    </li>
                </ul>

            </section>
        </main>

        ${await Footer.create(contexts, lang, pageName)}
    `
}

const render = defaultRender(html, defaultHead(pageName))

export {
    render
}
