
import { Header, Footer } from '../components'
import stylesheet from './contact.module.css?inline'
import styles from './contact.module.css'
import { init as initI18N } from '../helpers/i18n'
import { defaultRender, defaultHead } from '../helpers/page'


const pageName = 'contact'

const externalLinkSVG = (i18n) => `
    <svg width="12" height="12" viewBox="0 0 24 24">
        <title>${i18n.externallinks}</title>
        <path d="M14,3V5H17.59L7.76,14.83L9.17,16.24L19,6.41V10H21V3M19,19H5V5H12V3H5C3.89,3 3,3.9 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V12H19V19Z"></path>
    </svg>
`

const html = async (lang) => {
    const contexts = (await initI18N(lang))
    const i18n = contexts.translate(pageName)
    const externalLinkIcon = externalLinkSVG(i18n)

    return `
        ${Header.create(contexts, 'how-to-find-me', lang)}
        
        <style>${stylesheet}</style>
        <main>        
            <section
                id="how-to-find-me"
                class="${styles.howtofindme} h-card vcard">
               
                <h1>
                    <a href="#how-to-find-me">${i18n.talktome}</a>
                </h1>
                <p>${i18n.bestwaytotalk}</p>
                <p>${i18n.emailtip}</p>
                <p >${i18n.pubkey}</p>
                <small>
                <code class="u-key key">
<p>-----BEGIN PGP PUBLIC KEY BLOCK-----</p>
<br />
<p>mDMEZbVoDBYJKwYBBAHaRw8BAQdAR93jmPTx8DSGOhi6W7W30fq4ekZns2RuasrW</p>
<p>+q1WtJ+0HlJlbmllIDxyZW5pZWRldi5zaXRlQDNuaWUuY29tPoiTBBMWCgA7FiEE</p>
<p>zeTcDbz4tAwa7R9mZmq9AT4in+IFAmW1aAwCGwMFCwkIBwICIgIGFQoJCAsCBBYC</p>
<p>AwECHgcCF4AACgkQZmq9AT4in+LkywEAid7GcqvX46OfC40k2UlVl/yMoIh8/Apt</p>
<p>zbFQ8I+fCDoBAKob/H7/I7Yj3MkLMv+Zyl8DU5UPq937R2MI3vzIVrgOuDgEZbVo</p>
<p>DBIKKwYBBAGXVQEFAQEHQACqmPGZcqHv3Rta1+WBV3a9oUsuo051FacZJbBjGoMR</p>
<p>AwEIB4h4BBgWCgAgFiEEzeTcDbz4tAwa7R9mZmq9AT4in+IFAmW1aAwCGwwACgkQ</p>
<p>Zmq9AT4in+IPJgD/a1SeAgeDSs4l+YTJU+jSLSaO8rDG2m2iII27/LXZ1fMA/07H</p>
<p>URUC9TDMQnu32BCmAk0bmZjvSbwY1Ate+ucry5sI<br /p>
<p>=DlHk</p>
<p>-----END PGP PUBLIC KEY BLOCK-----</p>
                </code>
                </small>
                <h2 id="links">
                    <a href="#links">${i18n.links}</a>
                </h2>
                <p>${i18n.meinotherplaces}<p>
                <ul>
                    <li>
                        <a class="u-url url" href="https://github.com/renie" rel="nofollow me">Github ${externalLinkIcon}</a>
                    </li>
                    <li>
                        <a class="u-url url" href="https://gitlab.com/renie" rel="nofollow me">Gitlab ${externalLinkIcon}</a>
                    </li>
                    <li>
                        <a class="u-url url" href="https://linkedin.com/in/reniesiqueira" rel="nofollow me">Linkedin ${externalLinkIcon}</a>
                    </li>
                    <li>
                        <a class="u-url url" href="https://www.codementor.io/@renie457" rel="nofollow me">Codementor ${externalLinkIcon}</a>
                    </li>
                </ul>

            </section>
        </main>

        ${await Footer.create(contexts, lang, pageName)}
    `
}

const render = defaultRender(html, defaultHead(pageName))

export {
    render
}
