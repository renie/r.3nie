import * as Header  from './header/header.js'
import * as Footer  from './footer/footer.js'

export {
    Header,
    Footer
}

