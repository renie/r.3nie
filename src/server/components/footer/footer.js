import stylesheet from './footer.module.css?inline'
import styles from './footer.module.css'
import { init as initI18N } from '../../helpers/i18n'

const create = async (_, lang, pageName = '') => {
    const contexts = (await initI18N(lang))
    const i18n = contexts.translate('footer')

    return `
        <style scoped>${stylesheet}</style>
        <footer class="${styles.footer}">
            <section id="lang-selector">
                <strong id="lang-title">${i18n.chooselanguage}</strong>
                <ul
                    role="listbox"
                    aria-labelledby="lang-title">
                    <li
                        aria-selected="${lang === 'en' ? 'true' : 'false'}" 
                        role="option">
                        <a
                            href="/en/${pageName}"
                            hreflang="en"
                            lang="en"
                            title="Change language to English.">
                            English
                        </a>
                    </li>
                    <li
                        aria-selected="${lang === 'pt' ? 'true' : 'false'}" 
                        role="option">
                        <a
                            href="/pt/${pageName}"
                            hreflang="pt"
                            lang="pt"
                            title="Mudar idioma para Português.">
                            Português
                        </a>
                    </li>
                </ul>
            </section>
        </footer>
    `
}

export {
    create
}
