import stylesheet from './header.module.css?inline'
import styles from './header.module.css'
import * as Menu from './menu.js'

const create = (i18n, mainSectionId, lang) => {
    const translations = i18n.translate('header')
    return `
        <header>
            <style scoped>${stylesheet}</style>
            <a href="#${mainSectionId}" class="${styles.skip}" tabindex="0">${translations.skiptocontent}</a>

            <a href="#lang-selector" class="${styles.skip}">${translations.changelanguage}</a> 

            <a href="/${lang}/home" class="${styles.link}" title="${translations.backtohome}">
                <em class="${styles.logo}" translate="no">r.3nie.com</em>
            </a>

            ${Menu.create(i18n, lang)}
        </header>
    `
}

export {
    create
}
