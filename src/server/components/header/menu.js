import stylesheet from './menu.module.css?inline'
import styles from './menu.module.css'

const createMenuList = (translations) => ([
    {
        link: 'contact',
        title: translations.contacttitle,
        label: translations.contact
    },
    {
        link: 'projects',
        title: translations.projectstitle,
        label: translations.projects
    }
])

const createMenuComponent = (lang, menuItem) => `
    <li>
        <a
            href="/${lang}/${menuItem.link}"
            title="${menuItem.title}">
            ${menuItem.label}
        </a>
    </li>
`

const create = (i18n, lang) => {
    
    const menuItems = createMenuList(i18n.translate('mainmenu'))

    return `
        <style scoped>${stylesheet}</style>
        <nav class="${styles.menu}" aria-label="Main">
            <menu>
                ${menuItems
                    .map(createMenuComponent.bind(null, lang)).join('')}
            </menu>
        </nav>
    `
}

export {
    create
}
