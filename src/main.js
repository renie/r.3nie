import '@fontsource/open-sans'
import './style/root.css'
import { I18N } from '@helpers'
import { Home } from '@pages'

(async () => {
    await I18N.init()

    document
        .body
        .insertAdjacentHTML('afterbegin', Home.create())
})()

