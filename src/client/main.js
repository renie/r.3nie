const getPageName = () => {
    const pathname = window.location.pathname.replace('/', '')
    return pathname === ''
        ? 'home'
        : pathname
}

const loadPageScript = async (page) => (await import(`./pages/${page}.js`)).default

const loadPage = async () => {
    try {
        await loadPageScript(getPageName())
    } catch {}
}

await loadPage()
