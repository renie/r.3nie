import { defineConfig } from 'vite'

export default defineConfig({
    plugins: [
    ],
    resolve: {
        alias: {
            "@components": "/src/components",
            "@helpers": "/src/helpers",
            "@pages": "/src/pages",
        },
    },
    css: {
        modules: {
            localsConvention: 'camelCaseOnly'
        }
    },
    build: {
        ssrEmitAssets: true,
        target: "esnext"
    },
})
