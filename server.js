import express from 'express'
import { StatusCodes } from 'http-status-codes'

import env from './src/server/helpers/env.js'
import * as middlewares from './src/server/helpers/middlewares.js'
import { getAssets, prepareHtml } from './src/server/helpers/assets.js'


let viteInstance

const loadAssetsForPage = async (page, url) => {
    return await getAssets(env, url, page, viteInstance)
}

const setResponse = async (res, req, responseStatus, assets, url) => {
    const rendered = await assets.render(url, assets.ssrManifest, req.lang)
    const html = prepareHtml(assets.template, rendered, req.lang)

    const secondsInAnHour = 3600
    const aBitLessThanADay = secondsInAnHour * 23.5
    res.status(responseStatus)
        .set({ 'Content-Type': 'text/html' })
        .set('Cache-Control', `public, max-age=${aBitLessThanADay}, s-maxage=${aBitLessThanADay}`)
        .end(html)

    return true
}

const mainEndpoint = async (req, res) => {
    const url = req.originalUrl.replace(env.base, '')
    let responseStatusCode, assets

    if (req.error) {
        assets = await loadAssetsForPage('e404', url)
        responseStatusCode = StatusCodes.NOT_FOUND
        return await setResponse(res, req, responseStatusCode, assets, url)
    }

    try {
        assets = await loadAssetsForPage(req.nextPage, url)
        responseStatusCode = StatusCodes.OK
    } catch (e) {
        viteInstance?.ssrFixStacktrace(e)
        console.log(e.stack)
        
        assets = await loadAssetsForPage('e500', url)
        responseStatusCode = StatusCodes.INTERNAL_SERVER_ERROR
    } finally {
        return await setResponse(res, req, responseStatusCode, assets, url)
    }
}

const startServer = async (viteServer) => {
    let { app } = await middlewares.addAll(express(), env, viteServer)

    viteInstance = viteServer
    app.get('/:language/:page', mainEndpoint)
    app.get('/:bad', mainEndpoint)
    app.get('/', mainEndpoint)

    app.listen(env.port, () => {
        console.log(`Server started at ${env.port}`)
    })
}

export default startServer

