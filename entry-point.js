const createViteServer = async () => {
    const { createServer } = await import('vite')
    return await createServer({
        server: { middlewareMode: true },
        appType: 'custom',
        base: '/'
    })
}

const env = process.env.NODE_ENV
let vite = null
if (env !== 'production') vite = await createViteServer()

if (vite) (await vite.ssrLoadModule('server.js')).default(vite)
else (await import('./server.js')).default()
